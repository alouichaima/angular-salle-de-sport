import { Role } from "./role";

export class Utilisateur {
  id:number
  nom:string;
  prenom:string;
  datenaiss:Date
  email:string;
  password:string;
  telephone: number;
 poids: number;
 role:Role


  constructor(nom:string , prenom:string , datenaiss:Date,email: string, password: string,
    telephone: number,poids: number, role:Role){
         this.nom=nom;
         this.prenom=prenom;
         this.datenaiss=datenaiss;
         this.email=email;
         this.password=password;
         this.telephone=telephone;
         this.poids=poids;
       }
}
