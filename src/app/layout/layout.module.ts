import { AbonneLayoutComponent } from './abonne-layout/abonne-layout.component';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminLayoutComponent } from './admin-layout/admin-layout.component';
import { FrontLayoutComponent } from './front-layout/front-layout.component';
import { LoginAdminLayoutComponent } from './login-admin-layout/login-admin-layout.component';



@NgModule({
  declarations: [
    AdminLayoutComponent,
    FrontLayoutComponent,
    LoginAdminLayoutComponent,
    AbonneLayoutComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
  ]
})
export class LayoutModule { }
