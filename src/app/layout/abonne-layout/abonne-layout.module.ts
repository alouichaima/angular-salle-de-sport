import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AbonneLayoutRoutingModule } from './abonne-layout-routing.module';
import { AbonneLayoutComponent } from './abonne-layout.component';


@NgModule({
  declarations: [
    AbonneLayoutComponent
  ],
  imports: [
    CommonModule,
    AbonneLayoutRoutingModule,
    RouterModule,
  ]
})
export class AbonneLayoutModule { }
