import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AbonneLayoutComponent } from './abonne-layout.component';

describe('AbonneLayoutComponent', () => {
  let component: AbonneLayoutComponent;
  let fixture: ComponentFixture<AbonneLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AbonneLayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AbonneLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
