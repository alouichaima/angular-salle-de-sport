import { LayoutModule } from './layout/layout.module';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';
import { AuthInterceptor } from './serviceinsc/auth-interceptor';

@NgModule({
  declarations: [
    AppComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
    AppRoutingModule,
    LayoutModule,
    HttpClientModule,

  ],
  providers: [
    JwtHelperService ,
   {provide:HTTP_INTERCEPTORS ,useClass:AuthInterceptor , multi:true},
   {provide:JWT_OPTIONS , useValue:JWT_OPTIONS}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
