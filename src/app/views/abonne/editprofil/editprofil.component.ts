import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editprofil',
  templateUrl: './editprofil.component.html',
  styleUrls: ['./editprofil.component.scss']
})
export class EditprofilComponent implements OnInit {
  private router:Router

  constructor() { }

  ngOnInit(): void {
  }
  modifierMotDePasse(): void{
    this.router.navigate( ['changer-mot-passe']);

 }

}
