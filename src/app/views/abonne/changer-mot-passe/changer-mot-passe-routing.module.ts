import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChangerMotPasseComponent } from './changer-mot-passe.component';

const routes: Routes = [
  {path:'' , component:ChangerMotPasseComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChangerMotPasseRoutingModule { }
