import { EditpaComponent } from './../editpa/editpa.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserLayoutRoutingModule } from './user-layout-routing.module';
import { UserLayoutComponent } from './user-layout.component';


@NgModule({
  declarations: [
    UserLayoutComponent,
    EditpaComponent

  ],
  imports: [
    CommonModule,
    UserLayoutRoutingModule
  ]
})
export class UserLayoutModule { }
