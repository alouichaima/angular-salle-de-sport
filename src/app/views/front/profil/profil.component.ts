import { ChangerMotPasseComponent } from './../changer-mot-passe/changer-mot-passe.component';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit {

  constructor(
    private router:Router
  ) { }

  ngOnInit(): void {
  }
  modifierMotDePasse(): void{
     this.router.navigate( ['changer-mot-passe']);

  }

}
