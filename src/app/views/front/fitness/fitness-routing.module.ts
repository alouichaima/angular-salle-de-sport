import { NgModule } from '@angular/core';
import {FitnessComponent } from './fitness.component';

import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path:'', component:FitnessComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FitnessRoutingModule { }
