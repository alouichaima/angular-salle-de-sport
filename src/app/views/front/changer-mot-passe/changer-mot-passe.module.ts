import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChangerMotPasseRoutingModule } from './changer-mot-passe-routing.module';
import { ChangerMotPasseComponent } from './changer-mot-passe.component';


@NgModule({
  declarations: [
    ChangerMotPasseComponent
  ],
  imports: [
    CommonModule,
    ChangerMotPasseRoutingModule
  ]
})
export class ChangerMotPasseModule { }
