import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllutilisateursComponent } from './allutilisateurs.component';

describe('AllutilisateursComponent', () => {
  let component: AllutilisateursComponent;
  let fixture: ComponentFixture<AllutilisateursComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllutilisateursComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllutilisateursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
