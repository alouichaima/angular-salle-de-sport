import { Utilisateur } from './../../../modelinscr/utilisateur';
import { UtilisateurService } from './../../../serviceinsc/utilisateur.service';
import { Observable, Subscriber } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-allutilisateurs',
  templateUrl: './allutilisateurs.component.html',
  styleUrls: ['./allutilisateurs.component.scss']
})
export class AllutilisateursComponent implements OnInit {


  utilisateur: Observable<Utilisateur[]>;

  constructor(private utilisateurService: UtilisateurService,
              private router: Router) { }




  ngOnInit(): void {
    this.reloadData();
  }

  reloadData(): void {
    // @ts-ignore
    this.utilisateur = this.utilisateurService.getAllutilisateur();
  }


}
