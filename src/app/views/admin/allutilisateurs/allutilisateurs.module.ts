import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AllutilisateursRoutingModule } from './allutilisateurs-routing.module';
import { AllutilisateursComponent } from './allutilisateurs.component';


@NgModule({
  declarations: [
    AllutilisateursComponent
  ],
  imports: [
    CommonModule,
    AllutilisateursRoutingModule
  ]
})
export class AllutilisateursModule { }
