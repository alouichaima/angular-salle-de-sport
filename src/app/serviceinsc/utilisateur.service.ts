import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {
  urlpath: string;

  constructor(private http: HttpClient) {
    this.urlpath ='http://localhost:8086/utilisateur';

  }
  getAllutilisateur()
  {
    return this.http.get(this.urlpath + '/all');
  }
}
