import { Router } from '@angular/router';
// import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
// import { TokenStorageService } from '../token-storage-service.service';
import { SignUpInfo } from '../modelinscr/signup-info';
import { map } from 'rxjs/operators';
import { JwtResponse } from '../modelinscr/jwt-response';
import { TokenStorageService } from '../token-storage-service.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Role } from '../modelinscr/role';

const httpOptions={
  headers : new HttpHeaders ({'Content-Type': 'application/json'})
};
const TOKEN_KEY='AuthToken'

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  role:Role[];

  // role = new Role();




  private currentUserSubject : BehaviorSubject<any>;
  public currentUser:Observable<any>;
  private signupUrl= 'http://localhost:8086/api/auth/register';

  constructor(private http:HttpClient ,
    private jwtHelper:JwtHelperService,
               private tokenStorage : TokenStorageService ,
                private router : Router) {
                  this.role=[{id:1,  name:"abonne"},
                             {id:2, name:"entraineur"}
                ];


               }
      signUp(signupInfo :SignUpInfo){
        return this.http.post<JwtResponse>(this.signupUrl , signupInfo,httpOptions)
        .pipe(map(data =>{
          this.saveUserData(data);
          return data;
        }
          ));
      }
      private saveUserData(data){
        this.tokenStorage
      }

      listeRole(): Role[]{
        return this.role;
      }


}
